# MiniAspire
This MiniAspire provides core functions for a simple loan with fixed interest rate (5%). 
**In this quick version of MiniAspire, the function for user repayments and to verify status is still missing.** 

# Setup
1. Clone the repository
2. Insert the **absolute** path of SQLite database into the `.env` file (DB_DATABASE field)
3. Create/Reset database tables: `php artisan migrate:refresh`
4. Seed frequencies table: `php artisan db:seed`
5. Create the encryption keys needed to generate secure access tokens `php artisan passport:install`
6. Start the http server: `php artisan serve`

# API
* User signup
```
POST /api/auth/signup
Headers: Content-Type (application/json), X-Requested-With (XMLHttpRequest)
Payload: name (string), email (string), password (string), password_confirmation (string)
```

* User login
```
POST /api/auth/login
Headers: Content-Type (application/json), X-Requested-With (XMLHttpRequest)
Payload: email (string), password (string), remember_me (boolean)
```

* User logout
```
GET /api/auth/logout
Headers: Content-Type (application/json), X-Requested-With (XMLHttpRequest), Authorization (Bearer <token>)
```

* User details
```
GET /api/auth/user
Headers: Content-Type (application/json), X-Requested-With (XMLHttpRequest), Authorization (Bearer <token>)
```

* Insert a new loan
```
POST /api/auth/loans
Headers: Content-Type (application/json), X-Requested-With (XMLHttpRequest), Authorization (Bearer <token>)
Payload: amount (integer), duration (integer), frequency (monthly, fortnightly, weekly)
```

* Show all loans
```
GET /api/auth/loans
Headers: Content-Type (application/json), X-Requested-With (XMLHttpRequest), Authorization (Bearer <token>)
```

* Show one loan
```
GET /api/auth/loans/{id}
Headers: Content-Type (application/json), X-Requested-With (XMLHttpRequest), Authorization (Bearer <token>)
```
<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FrequenciesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('frequencies')->insert([
            'name' => 'monthly',
            'days' => 30,
        ]);

        DB::table('frequencies')->insert([
            'name' => 'fortnightly',
            'days' => 15,
        ]);

        DB::table('frequencies')->insert([
            'name' => 'weekly',
            'days' => 7,
        ]);
    }
}

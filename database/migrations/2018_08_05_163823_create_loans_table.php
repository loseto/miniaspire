<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loans', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->date('duration');
            $table->double('interest_rate');
            //TODO: $table->double('arrangement_fee');
            $table->unsignedDecimal('instalment');
            $table->unsignedDecimal('total_debt');
            $table->unsignedDecimal('remaining_debt');
            $table->unsignedInteger('borrower_id');
            $table->unsignedInteger('frequency_id');

            $table->foreign('borrower_id')->references('id')->on('borrowers');
            $table->foreign('frequency_id')->references('id')->on('frequencies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loans');
    }
}

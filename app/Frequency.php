<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Frequency extends Model
{
    public $timestamps = false;

    public function loans()
    {
        return $this->hasMany('App\Loan');
    }
}

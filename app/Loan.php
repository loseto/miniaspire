<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
    protected $fillable = [
        'duration', 'repayment_frequency', 'interest_rate', 'installment', 'total_debt', 'remaining_debt', 'instalment', 'borrower_id', 'frequency_id'
    ];

    public function borrower()
    {
        return $this->belongsTo('App\Borrower');
    }

    public function frequency()
    {
        return $this->belongsTo('App\Frequency');
    }
}

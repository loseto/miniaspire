<?php
/**
 * Created by PhpStorm.
 * User: claudio
 * Date: 05/08/18
 * Time: 21:28
 */

namespace App\Utilities;


use App\Borrower;
use App\Frequency;
use App\Loan;
use Illuminate\Support\Facades\DB;

class PersistenceServices
{
    public static function insertLoan($user, $amount, $duration, $frequencyName)
    {
        $instalment = LoanCalculatorEngine::getInstalment($amount, $duration, $frequencyName);
        $totalAmount = LoanCalculatorEngine::getTotalAmount($duration, $frequencyName, $instalment);
        $frequencyId = Frequency::where('name', $frequencyName)->first()->id;

        $loan = DB::transaction(function () use ($instalment, $frequencyId, $totalAmount, $duration, $user) {

            $borrow = Borrower::create([
                'user_id' => $user->id
            ]);

            $loan = Loan::create([
                'duration' => $duration,
                'instalment' => $instalment,
                'interest_rate' => 0.05,
                'total_debt' => $totalAmount,
                'remaining_debt' => 0,
                'borrower_id' => $borrow->id,
                'frequency_id' => $frequencyId
            ]);

            return $loan;
        }, 2);

        return $loan;
    }
}
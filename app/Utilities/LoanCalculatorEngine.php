<?php
/**
 * Created by PhpStorm.
 * User: claudio
 * Date: 05/08/18
 * Time: 20:11
 */

namespace App\Utilities;


use App\Frequency;
use Carbon\Carbon;

class LoanCalculatorEngine
{
    public static function getInstalment($amount, $duration, $frequencyName)
    {
        $numberInstalment = LoanCalculatorEngine::getNumberOfInstalment($duration, $frequencyName);

        $numerator = $amount * 0.05 / 12;

        $denominatorFirstComponent = pow(1 + (0.05 / 12), $numberInstalment);
        $denominator = 1 - (1 / $denominatorFirstComponent);


        return round($numerator / $denominator, 2);
    }

    public static function getNumberOfInstalment($durationMonths, $frequencyName)
    {
        $frequencyDays = Frequency::where('name', $frequencyName)->first()->days;
        return $durationMonths / ($frequencyDays / 30);
    }

    public static function getExtincionDate($duration)
    {
        return Carbon::now()->addMonths($duration)->toDateString();
    }

    public static function getTotalAmount($duration, $frequencyName, $instalment)
    {
        $numberInstalment = LoanCalculatorEngine::getNumberOfInstalment($duration, $frequencyName);
        return round($instalment * $numberInstalment, 2);
    }
}
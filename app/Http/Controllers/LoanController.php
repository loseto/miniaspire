<?php

namespace App\Http\Controllers;

use App\Loan;
use App\Utilities\LoanCalculatorEngine;
use App\Utilities\PersistenceServices;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class LoanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Loan::with('frequency')->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'amount' => 'required|integer|min:5000|max:50000',
            'duration' => 'required|integer|min:1|max:48',
            'frequency' => 'required|exists:frequencies,name',
        ]);

        $amount = $request->input('amount');
        $duration = $request->input('duration');
        $frequency = $request->input('frequency');

        $extincionDate = LoanCalculatorEngine::getExtincionDate($duration);
        $numberInstalment = LoanCalculatorEngine::getNumberOfInstalment($duration, $frequency);

        $user = $request->user();
        $loan = PersistenceServices::insertLoan($user, $amount, $duration, $frequency);

        return response()->json([
            'message' => 'Success',
            'id' => $loan->id,
            'totalDebt' => $loan->total_debt,
            'instalment' => $loan->instalment,
            'numberOfInstalments' => $numberInstalment,
            'startDate' => Carbon::now()->toDateString(),
            'endDate' => $extincionDate
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Loan $loan
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json(Loan::with('frequency')->find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Loan $loan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Loan $loan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Loan $loan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Loan $loan)
    {
        //
    }
}
